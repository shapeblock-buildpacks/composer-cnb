package main

import (
	"gitlab.com/shapeblock-buildpacks/composer-cnb/composer"

	"github.com/paketo-buildpacks/packit"
)

func main() {
	packit.Build(composer.Build())
}
