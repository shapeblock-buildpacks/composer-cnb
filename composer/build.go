package composer

import (
	"fmt"
	"io/ioutil"
	"os"
	"os/exec"
	"path/filepath"
	"strings"

	"github.com/paketo-buildpacks/packit"
)

func Build() packit.BuildFunc {
	return func(context packit.BuildContext) (packit.BuildResult, error) {
		composerLayer, err := context.Layers.Get("composer")
		if err != nil {
			return packit.BuildResult{}, err
		}

		binPath := strings.Join([]string{os.Getenv("PATH"), filepath.Join(os.Getenv("PHP_HOME"), "bin")}, string(os.PathListSeparator))
		err = os.Setenv("PATH", binPath)
		if err != nil {
			return packit.BuildResult{}, err
		}
		// TODO: check if right version of composer exists before installing.
		composerBinary := filepath.Join(composerLayer.Path, "composer")

		matches, err := filepath.Glob(os.Getenv("PHP_HOME") + "/lib/php/extensions/no-debug-non-zts-*")
		if err != nil {
			return packit.BuildResult{}, err
		}
		fmt.Printf("%v", matches)
		// composer.php.ini
		phpIniConfig := PhpIniConfig{
			PhpHome:      os.Getenv("PHP_HOME"),
			ExtensionDir: matches[0],
			Extensions: []string{
				"bz2",
				"curl",
				"dba",
				"enchant",
				"fileinfo",
				"gd",
				"ldap",
				"mbstring",
				"openssl",
				"pdo",
				"pdo_mysql",
				"pdo_sqlite",
				"readline",
				"soap",
				"sodium",
				"sockets",
				"xsl",
				"zip",
				"zlib",
			},
			ZendExtensions: []string{
				"xdebug",
			},
		}

		composerPhpIni := filepath.Join(composerLayer.Path, "composer.php.ini")
		err = ProcessTemplateToFile(PhpIniTemplate, composerPhpIni, phpIniConfig)
		if err != nil {
			return packit.BuildResult{}, err
		}

		os.Setenv("PHPRC", composerPhpIni)

		if !fileExists(composerBinary) {
			fmt.Printf("Installing composer\n")

			downloadDir, err := ioutil.TempDir("", "downloadDir")
			if err != nil {
				return packit.BuildResult{}, err
			}
			defer os.RemoveAll(downloadDir)

			uri := "https://getcomposer.org/installer"
			err = exec.Command("curl",
				"-sS",
				uri,
				"-o", filepath.Join(downloadDir, "install_composer.php"),
			).Run()
			if err != nil {
				return packit.BuildResult{}, err
			}
			composerBin := filepath.Join(composerLayer.Path, "bin")
			os.Mkdir(composerBin, 0644)
			cmd := exec.Command("php",
				filepath.Join(downloadDir, "install_composer.php"),
				"--filename=composer",
				"--2", // make it "--2" if dependencies.php "composer/composer": "^2"
			)
			cmd.Stdout = os.Stdout
			cmd.Stderr = os.Stderr
			if err = cmd.Run(); err != nil {
				return packit.BuildResult{}, err
			}
			cmd = exec.Command("mv", "composer", composerLayer.Path)
			cmd.Stdout = os.Stdout
			cmd.Stderr = os.Stderr
			if err = cmd.Run(); err != nil {
				return packit.BuildResult{}, err
			}
		}

		os.Setenv("COMPOSER_HOME", filepath.Join(composerLayer.Path, ".composer"))
		os.Setenv("COMPOSER_CACHE_DIR", filepath.Join(composerLayer.Path, "cache"))

		binPath = strings.Join([]string{os.Getenv("PATH"), composerLayer.Path}, string(os.PathListSeparator))
		err = os.Setenv("PATH", binPath)
		if err != nil {
			return packit.BuildResult{}, err
		}

		composerLayer.Build = true
		composerLayer.Launch = true
		composerLayer.Cache = true

		fmt.Printf("Running composer install\n")

		// composer install
		cmd := exec.Command(
			"composer",
			"install",
			"--no-ansi",
			"--no-interaction",
			"--no-progress",
			"--prefer-dist",
			"--optimize-autoloader",
		)
		cmd.Stdout = os.Stdout
		cmd.Stderr = os.Stderr
		if err = cmd.Run(); err != nil {
			return packit.BuildResult{}, err
		}

		return packit.BuildResult{
			Plan: context.Plan,
			Layers: []packit.Layer{
				composerLayer,
			},
		}, nil
	}
}
