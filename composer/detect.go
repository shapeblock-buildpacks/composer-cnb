package composer

import (
	"fmt"
	"io/ioutil"
	"os"
	"path/filepath"

	"github.com/paketo-buildpacks/packit"
	"gopkg.in/yaml.v2"
)

type PlatformConfig struct {
	Name  string `yaml:"name"`
	Type  string `yaml:"type"`
	Build struct {
		Flavor string `yaml:"flavor"`
	} `yaml:"build,omitempty"`
	Web struct {
		Commands struct {
			Start string `yaml:"start"`
		} `yaml:"commands"`
	} `yaml:"web"`
	X map[string]interface{} `yaml:"-"`
}

func fileExists(filename string) bool {
	info, err := os.Stat(filename)
	if os.IsNotExist(err) {
		return false
	}
	return !info.IsDir()
}

func Detect() packit.DetectFunc {
	return func(context packit.DetectContext) (packit.DetectResult, error) {

		yamlFile, err := ioutil.ReadFile(filepath.Join(context.WorkingDir, ".platform.app.yaml"))
		if err != nil {
			return packit.DetectResult{}, err
		}
		var config PlatformConfig

		err = yaml.Unmarshal(yamlFile, &config)
		if err != nil {
			return packit.DetectResult{}, err
		}

		fmt.Printf("Build flavor -> %s\n", config.Build.Flavor)
		if config.Build.Flavor != "composer" {
			return packit.DetectResult{}, fmt.Errorf("not composer flavored build.")
		}

		composerJson := filepath.Join(context.WorkingDir, "composer.json")

		if !fileExists(composerJson) {
			return packit.DetectResult{}, fmt.Errorf("no composer.json found.")
		}

		return packit.DetectResult{
			Plan: packit.BuildPlan{
				Provides: []packit.BuildPlanProvision{
					{Name: "composer"},
				},
				Requires: []packit.BuildPlanRequirement{
					{
						Name: "composer",
					},
				},
			},
		}, nil
	}
}
